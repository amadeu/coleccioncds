/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coleccioncds;

/**
 *
 * @author Grupo2
 */
public class Persona {
    
 private   int idPersonas;
   private String nombre;
   private String apellidos;
   private String dni;
   private int edad;

    @Override
    public String toString() {
        return "Persona{" + "idPersonas=" + idPersonas + ", nombre=" + nombre + ", apellidos=" + apellidos + ", dni=" + dni + ", edad=" + edad + '}';
    }

    public Persona() {
    }

    public Persona(int idPersonas, String nombre, String apellidos, String dni, int edad) {
        this.idPersonas = idPersonas;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.edad = edad;
    }

    public int getIdPersonas() {
        return idPersonas;
    }

    public void setIdPersonas(int idPersonas) {
        this.idPersonas = idPersonas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    
    
}
