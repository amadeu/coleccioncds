
package clases;

public class Television extends Electrodomestico{
    float resolucion=20;
    boolean sintonizadorTDT=false;
	public Television() {}
    public Television(float precio_base, float peso) {
        super(precio_base, peso);
    }
    public Television(float precio_base, float peso, String color, char consumo_energetico, float resolucion, boolean sintonizadorTDT) {
        super(precio_base, peso, color, consumo_energetico);
        this.resolucion=resolucion;
        this.sintonizadorTDT=sintonizadorTDT;
    }
	 public float getResolucion() {
	        return resolucion;
	    }

	    public void setResolucion(float resolucion) {
	        this.resolucion = resolucion;
	    }

	    public void setSintonizadorTDT(boolean sintonizadorTDT) {
	        this.sintonizadorTDT = sintonizadorTDT;
	    }
	    

	    public boolean isSintonizadorTDT() {
	        return sintonizadorTDT;
	    }
	    @Override
	    public String getTipo() {
	    	return "TV";
	    }
	    @Override
	    public void precioFinal() {
	        super.precioFinal(); 
	        if (resolucion>40 ) {
	            precio_base+=precio_base*0.30/100;
	        }
	        if(sintonizadorTDT){
	            precio_base+=50;
	        }
	        
	    }
}
